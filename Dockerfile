FROM python:3.6-alpine

WORKDIR /app

COPY /app /app

RUN apk add libpq-dev build-base && \
	pip3 install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD ["sh", "entrypoint.sh"]

